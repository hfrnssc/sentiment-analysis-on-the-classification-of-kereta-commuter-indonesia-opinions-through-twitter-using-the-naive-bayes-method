import re
import pandas
import numpy
import string
import nltk
import Sastrawi
from textblob import TextBlob
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory, StopWordRemover, ArrayDictionary
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize import TweetTokenizer

#col_names = ["waktu","id_","uname","tw","kl_sen"]
#asto = pandas.read_csv('crawling_commuterline_percepatann.csv', names=col_names, sep=';', header)
factory = StopWordRemoverFactory()
stopword = factory.create_stop_word_remover()

factory2 = StemmerFactory()
stemmer = factory2.create_stemmer()

asp = pandas.read_csv('crawling_commuterline_percepatann.csv', sep=';')
print(asp.head())

#load dataset
#def load_data():
#    data = pandas.read_csv('crawling_commuterline_percepatann.csv', sep=';')
#    return data

#tweet_df = load_data()
#tweet_df.head()

#df = pandas.DataFrame(tweet_df[['waktu','uname','tw','kl_sen']])


##asp['preprocessing'] = asp['tw'].apply(lambda x: x.split())
#tt = TweetTokenizer()
#twtokn = tt
#asto['preprocessing'] = asto['tw'].apply(twtokn.tokenize)

asp['preprocessing'] = asp['tw'].apply(lambda x: " ".join(stopword.remove(x) for x in x.split()))
#asto['preprocessing'] = asto['tw'].apply(lambda x: " ".join(stopword.remove(x) for x in x.split()))
#stop_factory = StopWordRemoverFactory().get_stop_words()
#more_stopword = ['woooyyy']

#data = stop_factory + more_stopword

#dictionary = ArrayDictionary(data)
#str = StopWordRemover(dictionary)
#strng = str

#asto['preprocessing'] = asto['preprocessing'].apply(strng.remove)

stm = stemmer
asp['preprocessing'] = asp['preprocessing'].apply(stm.stem)
#asto['preprocessing'] = stemmer.stem(str(asto['preprocessing'])) : hasilnya berantakan
#yang lebih parah dari library R yang bernama Nurandi

asp['preprocessing'] = asp['preprocessing'].apply(lambda x: " ".join(re.sub("(b [A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",x).split()))

df = pandas.DataFrame()
df['waktu']=pandas.Series(asp['waktu'])
df['uname']=pandas.Series(asp['uname'])
df['tw']=pandas.Series(asp['preprocessing'] )
df['kl_pen']=pandas.Series(asp['kl_pen'])

  #"waktu:time","uname:username","tw:tweet","kl_pen:opinion classification (positive, negative, and neutral)"

df.to_csv('preprocessing_result.csv',encoding='utf8', index=False,sep=';')








